import { Route, Routes } from 'react-router';
import Homepage from './components/Homepage/Homepage';
import Review from './components/Review/Review';
import Login from './components/User/Login/Login';
import Register from './components/User/Register/Register';
import { AppRoutes } from './components/routes/AppRoutes';
// const Homepage = () => {
//   return (
//   <>
//    <Navbar/>
//    <Hero/>
//    <Trending/>
//    <Banner/>
//    <Discover/>
//    <Footer/>
//   </>
//   )
// }
const App = () => {
  return (
    <div className='App'>
      <AppRoutes/>
    </div>
  )
}

export default App