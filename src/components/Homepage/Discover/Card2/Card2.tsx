import '../Card2/card2.scss';
import imgIcon from '../../../../assets/images/Ellipse 8.png';

const Card2 = () => {
  return (
    <div className="cards2">
    <div className="person">
            <div className="author">
                <span> <img src={imgIcon} alt="" /> </span>
                <span> <p>John Doe</p></span>
            </div>
        <div className="time">23 min ago</div>
    </div>
    <div className="subject-title">
      Growing up with Several Passions
    </div>
    <div className="subject-review">
    As I was growing up, I found myself indulging in all sorts of things. From arts
to music ...
    </div>
        <div className="stars">
            <i className="fa-regular fa-star"></i>
            <i className="fa-regular fa-star"></i>
            <i className="fa-regular fa-star"></i>
            <i className="fa-regular fa-star"></i>
        </div>
        
    </div>
  )
}

export default Card2