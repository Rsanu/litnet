import '../Discover/discover.scss';
import Card2 from './Card2/Card2';

import Card3 from './Card3/Card3';

const Discover = () => {
    return (
        <section className="discover">
            <div className="container">
                <div className="left">
                    <div className="rising">
                        <div className="text">Up and Rising</div>
                        <div className="view">View all</div>
                    </div>
                   <Card2/>
                   <Card2/>
                   <Card2/>
                   
                </div>
                <div className="right">
                    <div className="heading">
                    Discover what you like
                    </div>
                    <div className="hashtags">
                        <div>Scifi</div>
                        <div>Comedy</div>
                        <div>History</div>
                        <div>Comics and Movies</div>
                        <div>Career</div>
                        <div>College life</div>
                    </div>
                    <div className="heading sub-title">
                    Top writers this month
                    </div>
                   <Card3/>
                   <Card3/>
                   <Card3/>
                   <Card3/>

                </div>
            </div>
        </section>
    )
}

export default Discover