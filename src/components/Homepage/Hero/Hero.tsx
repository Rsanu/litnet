import '../Hero/hero.scss';
import HeroImg from '../../../assets/images/Frame.png';

export const Hero = () => {
    return (
        <section className="hero">
            <div className="container">
                <div className="image-area">
                    <img src={HeroImg} alt="img hero" />

                </div>
                <div className="text-area">
                    <div className="text-contents">
                        <div className='title'>Express your</div>
                        <div className='para'>Inner writer</div>
                        <div className="buttons">
                            <button className='hero-btn1'> Join Poem Net</button>
                            <button className='hero-btn'> Join Poem Net</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}
