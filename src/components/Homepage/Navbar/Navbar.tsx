import '../Navbar/navbar.scss';
import Logo from "../../../assets/images/LitNet.png";
import { BiSearch } from "react-icons/bi";
import { NavLink } from 'react-router-dom';


const Navbar = () => {
  return (
    <nav>
      <div className='container'>
        <div className='navbar'>
          <div className='logo'>
            <NavLink to={'/'}><img src={Logo} alt="logo" /></NavLink>
          </div>
          <div className='input-container'>
            <BiSearch />
            <input type="text" placeholder='Search' />
          </div>
          <div className='nav-links'>
            <div><NavLink to={'/catagories'}>Catagories</NavLink></div>
            <div> <NavLink to={'/login'}>Login</NavLink></div>
            <div> <NavLink to={'/register'}>Register</NavLink></div>

          </div>

        </div>
      </div>
    </nav>
  )
}

export default Navbar