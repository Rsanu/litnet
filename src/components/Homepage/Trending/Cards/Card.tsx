
import '../Cards/card.scss';
import imgIcon from '../../../../assets/images/Ellipse 8.png';

export const Card = () => {
  return (
    <div className="cards">
    <div className="sub-title">
      <div className="num">01</div>
      <div className="card-title">College Vibes</div>
    </div>
    <div className="stars">
      <i className="fa-regular fa-star"></i>
      <i className="fa-regular fa-star"></i>
      <i className="fa-regular fa-star"></i>
      <i className="fa-regular fa-star"></i>
    </div>
    <div className="name">
      <div className="author">
       <span> <img src={imgIcon} alt="" /></span>
       <span> <p>John Doe</p></span>
      </div>
      <div className="time">23 min ago</div>
    </div>
  </div>
  )
}
