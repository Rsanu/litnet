
import '../Trending/trending.scss';

import { Card } from './Cards/Card';


export const Trending = () => {
  return (
    <section className='trending'>
      <div className="container">
        <div className="title">Trending on LitNet</div>
        <div className="all-cards">
        <Card/>
        <Card/>
        <Card/>
        <Card/>
        <Card/>
        <Card/>
        </div>
      </div>

    </section>
  )
}
