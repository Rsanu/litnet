import { classicNameResolver } from 'typescript';
import { Card } from '../../Homepage/Trending/Cards/Card';
import './more.scss';

const More = () => {
  return (
    <section className='more'>
      <div className='trending'>
        <div className="container">
          <div className="title">More from Jane Doe</div>
          <div className="all-cards ">
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />

          </div>

        </div>

      </div>
    </section>
    

  )
}

export default More