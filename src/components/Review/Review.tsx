import Footer from '../../components/Homepage/Footer/Footer';
 import  Navbar  from '../../components/Homepage/Navbar/Navbar';
import More from '../../components/Review/More/More';
import'../Review/review.scss';
import Rockpool from './Rockpool/Rockpool';

const Review = () => {
  return (
   <>
    <Navbar/>
    <Rockpool/>
    <More/>
    <Footer/>
    </>
  )
}

export default Review