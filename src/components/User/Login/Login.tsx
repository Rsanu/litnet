import "../Login/login.scss";
import Image from '../../../assets/images/Frame1.png';
import Navbar from "../../Homepage/Navbar/Navbar";
import {AiFillLock} from 'react-icons/ai'
import {AiFillMail} from 'react-icons/ai'

const Login = () => {
    return (
        <section className="login-page">
            <Navbar/>
            <div className="container">
                <div className="image-area">
                    <img src={Image} alt="" />
                </div>
                <div className="login-area">
                    <div className="user-login">
                        <div className="login-div">
                        <h1>User Login</h1>
                        <div className="bar"></div>
                        <div className="input-field">
                        <div className="Email">
                        <AiFillMail/>
                            <input type="email" placeholder="Email Address" />

                        </div>
                        <div className="Password">
                        <AiFillLock/>
                            <input type="password" placeholder="Password" />
                        </div>
                        </div>
                        <div className="Forget">Forgot Password?</div>
                        <div className="button">
                        <button className="login-btn">Login</button>
                        </div>
                        <div className="Create">Create and Account</div>

                    </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Login