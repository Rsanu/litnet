import "../Register/register.scss";
import Image from '../../../assets/images/Frame1.png';
import Navbar from "../../Homepage/Navbar/Navbar";
import {AiFillLock} from 'react-icons/ai'
import {AiFillMail} from 'react-icons/ai'
import {BsFillPersonFill} from 'react-icons/bs'

const Register = () => {
    return (
        <section className="register-page">
            <Navbar/>
            <div className="container">
                <div className="image-area">
                    <img src={Image} alt="" />
                </div>
                <div className="register-area">
                    <div className="user-register">
                        <div className="register-div">
                        <h1>Register</h1>
                        <div className="bar"></div>
                        <div className="input-field">
                            <div className="Name">
                            <BsFillPersonFill/>
                            <input type="text" placeholder="Full name" />
                            </div>
                        <div className="Email">
                        <AiFillMail/>
                            <input type="email" placeholder="Email Address" />

                        </div>
                        <div className="Password">
                        <AiFillLock/>
                            <input type="password" placeholder="Password" />
                        </div>
                        <div className="ConfirmPassword">
                        <AiFillLock/>
                            <input type="password" placeholder="Confirm Password" />
                        </div>
                        </div>
                        <div className="Forget">Forgot Password?</div>
                        <div className="button">
                        <button className="Signup-btn">Sign up</button>
                        </div>
                        <div className="Create">Already Have an account?Sign in</div>

                    </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Register