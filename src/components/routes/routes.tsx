import Homepage from "../Homepage/Homepage";
import Review from "../Review/Review";
import Login from "../User/Login/Login";
import Register from "../User/Register/Register";

export const ROUTES=[
   
    {path:"/", element:<Homepage/>},
    {path:"/homepage", element:<Homepage/>},
    {path:"/login" ,element:<Login/>},
    {path:"/register" , element:<Register/>},
    {path:"/catagories" , element:<Review/>},
    {path:"*" , element:<div className='page'>Page not found</div>}

    

]